pub mod gandi;

use std::net::{Ipv4Addr, Ipv6Addr};

use anyhow::Result;
use async_trait::async_trait;

#[async_trait]
pub trait DnsProvider: Send + Sync {
    fn provider(&self) -> &'static str;
    async fn update_a(
        &self,
        domain: &str,
        subdomain: Option<&str>,
        targets: &[Ipv4Addr],
    ) -> Result<()>;
    async fn update_aaaa(
        &self,
        domain: &str,
        subdomain: Option<&str>,
        targets: &[Ipv6Addr],
    ) -> Result<()>;
    async fn update_cname(&self, domain: &str, subdomain: Option<&str>, target: &str)
        -> Result<()>;
}

impl std::fmt::Debug for dyn DnsProvider {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "DnsProvider({})", self.provider())
    }
}
