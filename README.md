D53
===

[![status-badge](https://woodpecker.deuxfleurs.fr/api/badges/39/status.svg)](https://woodpecker.deuxfleurs.fr/repos/39)

D53 is a dynamic DNS updater that sources information from Consul to route services to the correct place

### Tag syntax

D53 reads tags affected to services in Consul. Tags can be formatted as follows:

- `d53-a <domain> <subdomain>`, will set the node running this service as an IPv4 target in an A record for `<subdomain>.<domain>`
- `d53-aaaa <domain> <subdomain>`, same but as an IPv6 target in a AAAA record
- `d53-cname <domain> <subdomain>`, same but as an alias using a CNAME record

Example Nomad service configurations:

```hcl
# The following can be used in the Tricot service definition
# to redirect the current deuxfleurs.fr and <site_name>.site.deuxfleurs.fr
# to this node through A and AAAA records
    tags = [
           "(diplonat (tcp_port 80))"
           "d53-a ${meta.site}.site.deuxfleurs.fr",
           "d53-a global.site.deuxfleurs.fr",
           "d53-aaaa ${meta.site}.site.deuxfleurs.fr",
           "d53-aaaa global.site.deuxfleurs.fr",
         ]
```

```hcl
# The following can be used in the Guichet service definition
# to configure a Tricot reverse proxy entry, and to redirect using
# a CNAME the guichet.deuxfleurs.fr to the correct target,
# which is usually defined in the form of <site_name>.site.deuxfleurs.fr
    tags = [
           "tricot guichet.deuxfleurs.fr",
           "d53-cname guichet.deuxfleurs.fr",
         ]
```

### Finding targets

The IPv4, IPv6 and CNAME targets to set in the record are extracted from the metadata values affected to each node in the Consul configuration.
In particular, the following values are used:

- `public_ipv4`: a public IPv4 through which the current node is possibly reachable (see DiploNAT to automatically open ports in a NAT)
- `public_ipv6`: a public IPv6 through which the current node is reachable
- `cname_target`: a CNAME target that resolves to a domain name whose A and/or AAAA entries point to this node (possibly among others)

Here is the relevant Nix configuration snippet that sets up these metadata values:

```nix
    let node_meta = {
        "site" = cfg.site_name;
        "public_ipv6" = cfg.ipv6;
      } //
      (if cfg.public_ipv4 != null
        then { "public_ipv4" = cfg.public_ipv4; }
        else {}) //
      (if cfg.cname_target != null
        then { "cname_target" = cfg.cname_target; }
        else {});

    ### ... later ...
    services.consul.extraConfig.node_meta = node_meta;
```

### Example DNS layout for a Deuxfleurs cluster (see nixcfg repo for global setup)

- All IPv4 and IPv6 addresses of running Tricot nodes are registered using D53
  for the root domain, e.g. `deuxfleurs.fr`

- The IPv4 and IPv6 addresses of running Tricot nodes in each site are
  registered using D53 for the subdomain `<site_name>.site.<domain_name>`, e.g.
  `neptune.site.deuxfleurs.fr`

- For subdomains of global HTTP services (e.g. Garage), a CNAME entry is made
  by hand to the root domain , e.g. `garage.deuxfleurs.fr IN CNAME
  deuxfleurs.fr`

- For subdomains of HTTP services that run only at one place at once (e.g.
  Guichet, Grafana, Synapse, ...), a CNAME entry is registered automatically
  using D53 to the subdomain of the site on where it is running.
  In this case, users will be routed to any Tricot daemon running on one of
  the nodes of the site, which will then proxy the request to the final
  destination node through Wireguard.

- For non-HTTP services such as e-mail, a specific subdomain such as
  `smtp.deuxfleurs.fr` is created and populated with the correct IPv4 and IPv6
  addresses by D53 tags on the SMTP server's Consul service. Then, the `MX`
  entry is made to point to this dedicated subdomain.
