FROM rust:1.65-buster as builder

RUN apt-get update && \
    apt-get install -y libssl-dev pkg-config

WORKDIR /srv

# Build dependencies and cache them
COPY Cargo.* ./
RUN mkdir -p src && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -r src && \
    rm target/release/deps/d53*

# Build final app
COPY ./src ./src
RUN cargo build --release

FROM debian:bullseye-slim
RUN apt-get update && apt-get install -y libssl1.1 iptables ca-certificates
COPY --from=builder /srv/target/release/d53 /usr/local/sbin/d53
CMD ["/usr/local/sbin/d53"]
