#!/bin/sh

RUST_LOG=d53=debug cargo run \
	-- \
	--consul-addr http://localhost:8500 \
	--providers deuxfleurs.org:gandi \
	--gandi-api-key $GANDI_API_KEY \
	--allowed-domains staging.deuxfleurs.org
